package weather_api

import (
	"encoding/json"
	"errors"
	"fmt"
	"git.foxminded.com.ua/foxstudent102518/weather-bot/config"
	"io/ioutil"
	"net/http"
)

type main struct {
	Temp      float64 `json:"temp"`
}

type Weather struct {
	ID          int16  `json:"id"`
	Description string `json:"description"`
}

type forecastItem struct {
	Dt         int64     `json:"dt"`
	Main       main      `json:"main"`
	Weather    []Weather `json:"weather"`
}

type city struct {
	Name       string `json:"name"`
}

type WeatherForecast struct {
	List    []forecastItem `json:"list"`
	City    city           `json:"city"`
}

type WeatherApi struct {
	Cnf        config.Config
	HttpClient *http.Client
}

func (wa *WeatherApi) GetDailyForecastForWeek(lat, lon float32) (WeatherForecast, error) {
	url := fmt.Sprintf(wa.Cnf.WeatherApiUrl+"/data/2.5/forecast?lat=%f&lon=%f&units=%s&lang=%s&appid=%s",
		lat,
		lon,
		"metric",
		"ua",
		wa.Cnf.WeatherApiKey)
	resp, err := wa.HttpClient.Get(url)
	if err != nil {
		return WeatherForecast{}, err
	}
	if resp.StatusCode != 200 {
		return WeatherForecast{}, errors.New(resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return WeatherForecast{}, err
	}

	//var forecast map[string]interface{}
	var forecast WeatherForecast
	err = json.Unmarshal(body, &forecast)
	if err != nil {
		return WeatherForecast{}, err
	}
	return forecast, nil
}
