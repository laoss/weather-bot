package tgbot

import (
	"git.foxminded.com.ua/foxstudent102518/weather-bot/config"
	"git.foxminded.com.ua/foxstudent102518/weather-bot/loger"
	"git.foxminded.com.ua/foxstudent102518/weather-bot/txt"
	"git.foxminded.com.ua/foxstudent102518/weather-bot/weather_api"
	"github.com/rs/zerolog/log"
	tele "gopkg.in/telebot.v3"
	"net/http"
	"time"
)

func SetupNewBot(cnf config.Config) (*tele.Bot, error) {
	pref := tele.Settings{
		Token:  cnf.BotToken,
		Poller: &tele.LongPoller{Timeout: 10 * time.Second},
	}

	b, err := tele.NewBot(pref)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func CommandHandler(b *tele.Bot) {
	b.Handle(tele.OnText, func(c tele.Context) error {
		loger.LogSender(c)

		switch c.Text() {
		case "/start":
			return c.Send(txt.Start, tele.ModeHTML)
		default:
			return c.Send(txt.ErrLocation, tele.ModeHTML)
		}
	})
	b.Handle(tele.OnLocation, locationHandler)
}

func locationHandler(c tele.Context) error {
	loger.LogSender(c)

	loc := c.Message().Location
	if loc == nil {
		return c.Send(txt.ErrLocation, tele.ModeHTML)
	}
	wApi := weather_api.WeatherApi{Cnf: config.GetConfig(), HttpClient: http.DefaultClient}
	forecast, err := wApi.GetDailyForecastForWeek(loc.Lat, loc.Lng)
	if err != nil {
		log.Err(err).Msg("Failed to get daily forecast")
		return c.Send(txt.ErrWeatherForecast, tele.ModeHTML)
	}

	msg, err := txt.FormatForecast(forecast)
	if err != nil {
		return err
	}
	return c.Send(msg, tele.ModeHTML)
}
