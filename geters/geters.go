package geters

import (
	"git.foxminded.com.ua/foxstudent102518/weather-bot/weather_api"
)

func GetWeatherCondition(conditions []weather_api.Weather) (txt string, emj string) {
	var idx = 0

	for _, c := range conditions {
		if idx < 1 && c.ID == 800 { // clear sky
			idx = 1
			txt = c.Description
			emj = "☀️"
		} else if idx < 2 && c.ID == 801 { // few clouds: 11-25%
			idx = 2
			txt = c.Description
			emj = "🌤"
		} else if idx < 3 && c.ID == 802 { // scattered clouds: 25-50%
			idx = 3
			txt = c.Description
			emj = "⛅️"
		} else if idx < 4 && c.ID == 803 { // broken clouds: 51-84%
			idx = 4
			txt = c.Description
			emj = "🌥"
		} else if idx < 5 && c.ID == 804 { // overcast clouds: 85-100%
			idx = 5
			txt = c.Description
			emj = "☁️"
		} else if idx < 6 && c.ID > 700 && c.ID < 782 { // atmosphere
			idx = 6
			txt = c.Description
			emj = "🌫"
		} else if idx < 7 && c.ID > 199 && c.ID < 233 { // thunderstorm
			idx = 7
			txt = c.Description
			emj = "🌩"
		} else if idx < 8 && c.ID > 299 && c.ID < 322 { // drizzle
			idx = 8
			txt = c.Description
			emj = "🌧"
		} else if idx < 9 && c.ID > 499 && c.ID < 505 { // rain
			idx = 9
			txt = c.Description
			emj = "🌦"
		} else if idx < 10 && c.ID > 519 && c.ID < 532 { // shower rain
			idx = 10
			txt = c.Description
			emj = "🌧"
		} else if idx < 11 && c.ID == 511 { // freezing rain
			idx = 10
			txt = c.Description
			emj = "🌨"
		} else if idx < 12 && c.ID > 599 && c.ID < 623 { // snow
			txt = c.Description
			emj = "❄️"
			return
		}
	}
	return
}

func GetMinMax(a []float64) (min, max float64) {
	maxV := a[0]
	minV := a[0]
	for _, v := range a {
		if v > maxV {
			maxV = v
		}
		if v < minV {
			minV = v
		}
	}

	return maxV, minV
}
