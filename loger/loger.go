package loger

import (
	"git.foxminded.com.ua/foxstudent102518/weather-bot/config"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	tele "gopkg.in/telebot.v3"
	"os"
	"strconv"
)

func Setup(cnf config.Config) {
	if cnf.LogPretty == true {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}
	if cnf.LogTimeFormatUnix == true {
		zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	}
}

func LogSender(c tele.Context) {
	var (
		text = c.Text()
		user string
	)
	if c.Sender().Username != "" {
		user = c.Sender().Username
	} else {
		user = strconv.FormatInt(c.Sender().ID, 10) + "  " + c.Sender().FirstName
		if c.Sender().LastName != "" {
			user += " " + c.Sender().LastName
		}
	}

	log.Info().Msgf("user: %s, txt: %s", user, text)
}
