package main

import (
	"git.foxminded.com.ua/foxstudent102518/weather-bot/config"
	"git.foxminded.com.ua/foxstudent102518/weather-bot/loger"
	"git.foxminded.com.ua/foxstudent102518/weather-bot/tgbot"
	"github.com/rs/zerolog/log"
)

func main() {
	err := config.Load()
	if err != nil {
		log.Fatal().Err(err).Msg("Cannot load env variables.")
	}

	loger.Setup(config.GetConfig())

	bot, err := tgbot.SetupNewBot(config.GetConfig())
	if err != nil {
		log.Fatal().Err(err).Msg("Cannot setup new bot.")
	}

	tgbot.CommandHandler(bot)

	bot.Start()
}
