package txt

import (
	"fmt"
	"git.foxminded.com.ua/foxstudent102518/weather-bot/geters"
	"git.foxminded.com.ua/foxstudent102518/weather-bot/weather_api"
	"time"
)

const Start = `Привіт, це <b>Weather Bot</b>!
Відправ мені <u>геолокацію</u> і я покажу тобі прогноз погоди.`
const ErrLocation = `Це не геолокація.
Відправ геолокацію, щоб отримати прогноз погоди.`
const ErrWeatherForecast = `Не вдалось отримати прогноз погоди.
Спробуй пізніше знову.`

var Weekdays = [7]string{
	"Неділя",
	"Понеділок",
	"Вівторок",
	"Середа",
	"Четвер",
	"Пʼятниця",
	"Субота",
}
var MonthsShort = [12]string{
	"січ",
	"лют",
	"берез",
	"квіт",
	"трав",
	"черв",
	"лип",
	"серп",
	"верес",
	"жовт",
	"листоп",
	"груд",
}

func FormatForecast(forecast weather_api.WeatherForecast) (string, error) {
	city := forecast.City
	message := fmt.Sprintf("Прозноз погоди в місті <u>%s</u>.", city.Name)

	list := forecast.List
	date := time.Unix(list[0].Dt, 0)
	message += fmt.Sprintf("\n\n<pre>%s, %d %s</pre>", Weekdays[date.Weekday()], date.Day(), MonthsShort[date.Month()])
	var temperatures []float64
	var conditions []weather_api.Weather
	for _, fcItem := range list {
		fcTime := time.Unix(fcItem.Dt, 0)

		// in order to check the next day
		fcTime = time.Date(fcTime.Year(), fcTime.Month(), fcTime.Day(), 0, 0, 0, 0, time.Local)
		if fcTime.After(date) {
			printTempCond(&message, &temperatures, &conditions)

			date = fcTime
			message += fmt.Sprintf("\n\n<pre>%s, %d %s</pre>", Weekdays[date.Weekday()], date.Day(), MonthsShort[date.Month()])
		}

		fcMain := fcItem.Main
		temperatures = append(temperatures, fcMain.Temp)

		conditions = append(conditions, fcItem.Weather[0])
	}
	printTempCond(&message, &temperatures, &conditions)

	return message, nil
}

func printTempCond(msg *string, temps *[]float64, condits *[]weather_api.Weather) {
	tempMax, tempMin := geters.GetMinMax(*temps)
	condTxt, condEmj := geters.GetWeatherCondition(*condits)
	temps = nil
	condits = nil
	*msg += fmt.Sprintf("\n<b>🔼%.1f°   🔽%.1f°</b>", tempMax, tempMin)
	*msg += fmt.Sprintf("\n<i>%s   %s</i>", condTxt, condEmj)
}
