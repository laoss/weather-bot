package config

import (
	"errors"
	"github.com/joho/godotenv"
	"os"
	"strconv"
)

type Config struct {
	BotToken          string
	WeatherApiUrl     string
	WeatherApiKey     string
	LogPretty         bool
	LogTimeFormatUnix bool
}

var cnf Config

func Load() error {
	_ = godotenv.Load()

	ok := true
	cnf.BotToken, ok = os.LookupEnv("BOT_TOKEN")
	cnf.WeatherApiUrl, ok = os.LookupEnv("WEATHER_API_URL")
	cnf.WeatherApiKey, ok = os.LookupEnv("WEATHER_API_KEY")
	if !ok {
		return errors.New("missed some of env variable")
	}
	var err error
	cnf.LogPretty, err = strconv.ParseBool(os.Getenv("LOG_PRETTY_ENABLE"))
	if err != nil {
		return err
	}
	cnf.LogTimeFormatUnix, err = strconv.ParseBool(os.Getenv("LOG_TIME_FORMAT_UNIX"))
	if err != nil {
		return err
	}
	return nil
}

func GetConfig() Config {
	return cnf
}
